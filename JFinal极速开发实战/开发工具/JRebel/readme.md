
JRebel安装版本下载，可以修改版本号下载指定版本:
	
	http://update.zeroturnaround.com/update-site-archive/update-site-6.4.8.RELEASE/
	http://update.zeroturnaround.com/update-site-archive/update-site-6.5.2.RELEASE/
	
JRebel非安装版本下载：
	
	https://zeroturnaround.com/software/jrebel/download/prev-releases/
	
JRebel安装配置指南：

	http://manuals.zeroturnaround.com/jrebel/