/* Drop Tables */
DROP TABLE IF EXISTS test_blog;

/* Create Tables */
CREATE TABLE test_blog
(
	-- 主键
	ids varchar(32) NOT NULL,
	-- 标题
	title varchar(200),
	-- 内容
	content text,
	-- 创建时间
	createTime timestamp with time zone,
	PRIMARY KEY (ids)
) WITHOUT OIDS;

/* Comments */
COMMENT ON COLUMN test_blog.ids IS '主键';
COMMENT ON COLUMN test_blog.title IS '标题';
COMMENT ON COLUMN test_blog.content IS '内容';
COMMENT ON COLUMN test_blog.createTime IS '创建时间';
COMMENT ON TABLE test_blog IS '博客表';


